/**
 * 
 */
package com.rapidminer.beans.utils;

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.util.URLUtilities;

import com.petebevin.markdown.MarkdownProcessor;

/**
 * @author chris
 * 
 */
public class OperatorHelpFinder {

	static Logger log = LoggerFactory.getLogger(OperatorHelpFinder.class);

	public static String findOperatorHelp(Class<?> clazz,
			ClassLoader classloader) throws Exception {

		String className = clazz.getCanonicalName();
		String doc = "/" + className.replaceAll("\\.", "/") + ".md";
		URL url = clazz.getResource(doc);
		log.info("Operator-doc {} => {}", doc, url);
		if (url != null) {
			String txt = URLUtilities.readContent(url);
			log.debug("Found documentation at {}", url);

			MarkdownProcessor markdown = new MarkdownProcessor();
			String html = markdown.markdown(txt);

			if (html.startsWith("<h1>")) {
				int end = html.indexOf("</h1>");
				if (end > 0) {
					html = html.substring(end + "</h1>".length());
				}
			}

			log.trace("Html documentation:\n{}", html.trim());
			return html;
		}

		return null;
	}
}
