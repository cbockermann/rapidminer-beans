/**
 * 
 */
package com.rapidminer.beans.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.BodyContent;
import stream.runtime.DependencyInjection;
import stream.runtime.Variables;
import stream.runtime.setup.ParameterUtils;

import com.rapidminer.operator.Operator;
import com.rapidminer.operator.UserError;
import com.rapidminer.parameter.ParameterType;

/**
 * @author chris
 * 
 */
public class ParameterSetup {

	static Logger log = LoggerFactory.getLogger(ParameterSetup.class);

	public static void setParameters(Operator op) throws UserError {
		Map<String, ParameterType> types = ParameterTypeFinder
				.getParameterTypes(op.getClass());

		Map<String, String> parameters = new LinkedHashMap<String, String>();
		for (String name : types.keySet()) {
			String value = op.getParameter(name);
			if (value != null)
				parameters.put(name, value);
		}

		try {
			log.info("Injecting parameters {} into operator {}", parameters, op);
			inject(op, parameters, new Variables());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UserError(op, e.getMessage());
		}
	}

	/**
	 * This method injects a set of parameters to the given object.
	 * 
	 * @param o
	 *            The object to inject parameters into.
	 * @param params
	 *            The parameters to set on the object.
	 * @throws Exception
	 * @return Returns the parameter key that have been set using this method.
	 */
	public static Set<String> inject(Object o, Map<String, ?> params,
			Variables variableContext) throws Exception {
		log.debug("Injecting parameters {} into object {}", params, o);

		// this set contains a list of parameters that have been successfully
		// set using
		// accessible fields
		//
		Set<String> alreadySet = new HashSet<String>();

		Object embedded = params.get(BodyContent.KEY);

		// now, walk over all methods and check if one of these is a setter of a
		// corresponding
		// key value in the parameter map
		//
		for (Method m : o.getClass().getMethods()) {

			Class<?>[] t = m.getParameterTypes();

			if (DependencyInjection.isServiceSetter(m)) {
				log.debug("Skipping ServiceSetter '{}'", m.getName());
				continue;
			}

			if (embedded != null && m.getName().startsWith("set")
					&& t.length == 1 && t[0] == BodyContent.class) {
				log.debug("Setting embedded content...");
				m.invoke(o, new BodyContent(embedded.toString()));
				continue;
			}

			for (String k : params.keySet()) {

				if (m.getName().startsWith("set") && alreadySet.contains(k)) {
					log.debug(
							"Skipping setter '{}' for already injected field {}",
							m.getName(), k);
					continue;
				}

				//
				// if the method corresponds to a parameter of the map, try to
				// call it
				// with the appropriate value
				//
				if (m.getName().equalsIgnoreCase("set" + k)
						&& m.getParameterTypes().length == 1) {

					// Class<?> t = m.getParameterTypes()[0];

					if (t.equals(params.get(k).getClass())) {
						log.debug("Using setter '{}' to inject parameter '{}'",
								m.getName(), k);
						//
						// if the setter's argument type matches the value
						// object's class
						// in the parameter-map, we simply inject that object
						//
						m.invoke(o, params.get(k));
						alreadySet.add(k);

					} else {

						//
						// if the setter's argument does NOT match, we try to
						// create a new,
						// appropriate value for that setter using the
						// string-constructor
						// of the setter's argument type class
						//
						Object po = null;

						if (t[0].isPrimitive()) {
							String in = params.get(k).toString();

							if (t[0] == Double.TYPE)
								po = new Double(in);

							if (t[0] == Integer.TYPE)
								po = new Integer(in);

							if (t[0] == Boolean.TYPE)
								po = new Boolean(in);

							if (t[0] == Float.TYPE)
								po = new Float(in);

						} else {

							if (t[0].isArray()) {

								log.debug("setter is an array, using split(,) and array creation...");
								String[] args = ParameterUtils.split(params
										.get(k).toString());

								Class<?> content = t[0].getComponentType();
								Constructor<?> c = content
										.getConstructor(String.class);
								Object array = Array.newInstance(content,
										args.length);

								for (int i = 0; i < args.length; i++) {
									Object value = c.newInstance(args[i]);
									Array.set(array, i, value);
								}

								po = array;
							} else {

								try {
									Constructor<?> c = t[0]
											.getConstructor(String.class);
									po = c.newInstance(params.get(k).toString());
									log.debug("Invoking {}({})", m.getName(),
											po);
								} catch (NoSuchMethodException nsm) {
									log.error(
											"No String-constructor found for type {} of method {}",
											t, m.getName());
								}
							}
						}

						m.invoke(o, po);
						alreadySet.add(k);
					}
				}
			}
		}

		return alreadySet;
	}
}
