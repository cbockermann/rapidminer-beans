/**
 * 
 */
package com.rapidminer.beans.utils;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.BodyContent;
import stream.expressions.Condition;
import stream.runtime.setup.ParameterDiscovery;

import com.rapidminer.annotations.ParameterInfo;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeDouble;
import com.rapidminer.parameter.ParameterTypeFile;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeList;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.ParameterTypeText;
import com.rapidminer.parameter.TextType;

/**
 * This class implements a single utility method for retrieving the correct
 * RapidMiner ParameterType object from a given setter Method.
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
public class ParameterTypeFinder {
	static Logger log = LoggerFactory.getLogger(ParameterTypeFinder.class);

	public static Map<String, ParameterType> getParameterTypes(Class<?> clazz) {
		Map<String, ParameterType> types = discoverParameterTypes(clazz);
		return types;
	}

	/**
	 * Check the given class for any @parameter annotated fields.
	 * 
	 * @param clazz
	 * @return
	 */
	public static Map<String, ParameterType> discoverParameterTypes(
			Class<?> clazz) {

		log.debug("------------------------------------------------------------------------");
		log.debug("Exploring ParameterTypes for class '{}'", clazz);
		Map<String, ParameterType> types = new LinkedHashMap<String, ParameterType>();

		for (Field f : clazz.getDeclaredFields()) {
			log.debug("Checking field {}", f);

			ParameterMetaData info = getParameterInfo(f);
			if (info != null) {
				log.debug("Found annotated class attribute: {}", info);

				String name = f.getName();
				if (info.name() != null
						&& !"".equalsIgnoreCase(info.name().trim()))
					name = info.name().trim();

				ParameterType type = ParameterTypeFinder.getParameterType(info,
						name, f.getType());

				Method setter = getSetter(clazz, name, f.getType());
				if (setter == null) {
					log.error(
							"Found @Parameter annotated field '{}' but no set-method for that parameter!",
							f.getName());

					if (!Modifier.isPublic(f.getModifiers())) {
						log.error(
								"   field '{}' is non-public => parameter cannot be set!",
								f.getName());
					} else {
						log.debug(
								"Adding public annotated field '{}' as parameter!",
								f.getName());
						types.put(f.getName(), type);
					}

					continue;
				}

				if (type != null) {
					types.put(f.getName(), type);
				}
			}
		}

		for (Method m : clazz.getMethods()) {

			log.trace("Checking method {}", m);

			ParameterMetaData info = getParameterInfo(m);
			if (info != null && ParameterDiscovery.isSetter(m)) {
				log.debug("Found setter '{}'", m.getName());
				String key = m.getName().substring(3, 4).toLowerCase();

				if (types.containsKey(key)) {
					log.debug(
							"Already have annotated field for key '{}', skipping setter {}",
							key, m);
					continue;
				}

				if (m.getName().length() > 4)
					key += m.getName().substring(4);

				ParameterType type = ParameterTypeFinder.getParameterType(info,
						key, m.getParameterTypes()[0]);

				if (type != null) {
					log.debug("Adding parameter-type: {}", type);
					types.put(key, type);
					log.debug("  => parameter '{}'", key);
					types.put(key, type);
				}

				Class<?>[] t = m.getParameterTypes();
				if (t[0] == BodyContent.class) {
					log.debug("Found EmbeddedContent parameter, key = '{}'",
							key);
					type = new ParameterTypeText(key, "", TextType.JAVA);
					types.put(key, type);
					continue;
				}
			}
		}

		log.debug("------------------------------------------------------------------------");
		return types;
	}

	protected static ParameterMetaData getParameterInfo(Method m) {

		stream.annotations.Parameter param = m
				.getAnnotation(stream.annotations.Parameter.class);
		if (param != null)
			return new ParameterMetaData(param);

		ParameterInfo parameter = m.getAnnotation(ParameterInfo.class);
		if (parameter != null)
			return new ParameterMetaData(parameter);

		return null;
	}

	protected static ParameterMetaData getParameterInfo(Field f) {

		if (f.isAnnotationPresent(ParameterInfo.class)) {
			return new ParameterMetaData(f.getAnnotation(ParameterInfo.class));
		}

		return null;
	}

	/**
	 * This method determines the ParameterType of a setter method based on its
	 * argument type and the parameter annotation. If no parameter annotation is
	 * provided or the argument type cannot be mapped to a parameter type class,
	 * then <code>null</code> is returned.
	 * 
	 * @param param
	 * @param name
	 * @param type
	 * @return
	 */
	protected static ParameterType getParameterType(ParameterMetaData param,
			String name, Class<?> type) {

		if (param == null) {
			log.error("Cannot determine the parameter-type without an annotation!");
			return null;
		}

		String desc = "";
		ParameterType pt = null;
		Object defaultValue = param.defaultValue(); // parseDefaultValue(param.defaultValue(),
													// type);

		String key = name;
		if (param.name() != null) {
			key = param.name().trim();
			if (key.isEmpty())
				key = name;
		}

		if (param.description() != null) {
			desc = param.description();
		}

		//
		// String parameters
		//
		if (type.equals(String.class)
				|| type.equals(Condition.class)
				|| (type.isArray() && type.getComponentType().equals(
						String.class))) {
			log.debug("ParameterType is a String");

			if (param != null && param.values() != null)
				pt = new ParameterTypeString(key, desc, !param.required());
			else
				pt = new ParameterTypeString(key, desc, false);

			if (param != null && param.values() != null
					&& param.values().length > 0) {
				log.debug("Found category-parameter!");
				pt = new ParameterTypeCategory(key, desc, param.values(), 0);
			}
		}

		//
		// Parameters for doubles
		//
		if (type.equals(Double.class) || type.equals(double.class)
				|| type.equals(Float.class) || type.equals(float.class)) {
			log.debug("ParameterType {} is a Double!");

			pt = new ParameterTypeDouble(key, desc, param.min(), param.max(),
					!param.required());

			try {
				defaultValue = new Double(param.defaultValue());
			} catch (Exception e) {
				defaultValue = new Double(0.0d);
			}
		}

		//
		// Integer / Long parameters
		//
		if (type.equals(Integer.class) || type.equals(Long.class)
				|| type.equals(int.class) || type.equals(long.class)) {

			log.debug("ParameterType {} is an Integer!", type);

			Integer min = Integer.MIN_VALUE;
			Integer max = Integer.MAX_VALUE;

			if (param != null) {
				try {
					min = new Double(param.min()).intValue();
				} catch (Exception e) {
					min = Integer.MIN_VALUE;
				}
				try {
					max = new Double(param.max()).intValue();
				} catch (Exception e) {
					max = Integer.MAX_VALUE;
				}
			}

			pt = new ParameterTypeInt(key, desc, min, max, !param.required());
			if (param.defaultValue() != null) {

				String value = param.defaultValue();
				if (value.trim().isEmpty())
					value = "0";

				defaultValue = new Integer(value);
			}
		}

		//
		// Boolean parameters
		//
		if (type.equals(Boolean.class) || type.equals(boolean.class)) {
			log.debug("ParameterType {} is a Boolean!");
			pt = new ParameterTypeBoolean(key, desc, !param.required());

			try {
				defaultValue = new Boolean(param.defaultValue());
			} catch (Exception e) {
				defaultValue = false;
			}
		}

		if (type.equals(File.class)) {
			pt = new ParameterTypeFile(key, desc, null, !param.required());
		}

		//
		// Map parameters
		//
		if (Map.class.isAssignableFrom(type)) {
			log.debug("Found Map parameter... ");
			pt = new ParameterTypeList(key, desc, new ParameterTypeString(
					"key", ""), new ParameterTypeString("value", ""));
			return pt;
		}

		if (pt != null && defaultValue != null) {
			try {
				pt.setDefaultValue(defaultValue);
			} catch (Exception e) {
				log.error("Failed to set defaultValue of parameter '{}': {}",
						key, e.getMessage());
			}
		}

		return pt;
	}

	protected static Object parseDefaultValue(String defaultValue, Class<?> type) {
		try {
			if (defaultValue == null)
				return null;

			String value = defaultValue;

			if (defaultValue.trim().isEmpty()) {
				if (Number.class.isAssignableFrom(type)) {
					value = "0";
				}
			}
			Constructor<?> constructor = type.getConstructor(String.class);
			return constructor.newInstance(value);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Method getSetter(Class<?> targetClass, String parameterName,
			Class<?> parameterType) {

		for (Method m : targetClass.getMethods()) {

			if (!m.getName().equalsIgnoreCase("set" + parameterName)) {
				continue;
			}

			if (isSetterForType(m, parameterType)) {
				return m;
			}

		}

		return null;
	}

	public static boolean isSetterForType(Method m, Class<?> type) {
		if (!m.getName().startsWith("set"))
			return false;

		Class<?> types[] = m.getParameterTypes();
		if (types.length != 1)
			return false;

		return types[0].isAssignableFrom(type);
	}
}