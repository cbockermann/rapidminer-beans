RapidMiner Beans
================


Source Code & Usage
-------------------

The source code of the framework is available at
[bitbucket.org](http://bitbucket.org/cbockermann/rapidminer-beans).

The library is currently available via the following
maven repository:

      <repository>
         <id>jwall</id>
         <name>jwall.org Maven Repository</name>
         <url>http://secure.jwall.org/maven/repository/all</url>
      </repository>


