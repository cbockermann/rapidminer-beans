/**
 * 
 */
package com.rapidminer.beans;

import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rapidminer.beans.example.ExampleBeanOperator;
import com.rapidminer.beans.utils.ParameterTypeFinder;
import com.rapidminer.parameter.ParameterType;

/**
 * @author chris
 * 
 */
public class ParameterTypeFinderTest {

	static Logger log = LoggerFactory.getLogger(ParameterTypeFinderTest.class);

	@Test
	public void test() {

		Map<String, ParameterType> types = ParameterTypeFinder
				.getParameterTypes(ExampleBeanOperator.class);

		log.info("Found types: {}", types);
		for (String key : types.keySet()) {
			log.info("Found {} = {}", key, types.get(key));
		}

		Assert.assertEquals(2, types.size());
		Assert.assertTrue(types.containsKey("numberOfIterations"));
	}
}
