/**
 * 
 */
package com.rapidminer.beans.example;

import com.rapidminer.annotations.ParameterInfo;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;

/**
 * @author chris
 * 
 */
public class ExampleBeanOperator extends Operator {

	@ParameterInfo(name = "regex", description = "Some required String parameter", required = true)
	public String theString;

	@ParameterInfo(description = "the number of iterations", min = 1.0, max = 100.0, required = true)
	Integer numberOfIterations;

	/**
	 * @param description
	 */
	public ExampleBeanOperator(OperatorDescription description) {
		super(description);
	}

	/**
	 * @return the numberOfIterations
	 */
	public Integer getNumberOfIterations() {
		return numberOfIterations;
	}

	/**
	 * @param numberOfIterations
	 *            the numberOfIterations to set
	 */
	public void setNumberOfIterations(Integer numberOfIterations) {
		this.numberOfIterations = numberOfIterations;
	}

}
