%% PLEASE DON'T CHANGE BELOW
\documentclass[10pt, twoside]{article}
\usepackage[paperwidth=17cm,paperheight=24cm,textheight=18cm]{geometry}
\pagestyle{empty} % Header and footer set by master document
\date{}           % no date for article, only for proceedings
%% PLEASE DON'T CHANGE ABOVE

% START EDITING HERE


\usepackage{listings}
\usepackage{hyperref}
\usepackage{tikz}
\usetikzlibrary{shapes}



\usepackage{color}
\definecolor{rapidi}{RGB}{240,176,0}
\definecolor{rapidiText}{RGB}{102,51,0}
\definecolor{darkGreen}{RGB}{67,101,0}

\lstset{language=Java,
  basewidth={0.5em,0.45em},
  fontadjust=false,
  showspaces=false,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\ttfamily\color{black!60}       % comment style
%  stringstyle=\color{green}
}

% your title
\title{\LARGE \bfseries Get some Coffee for free -- Writing Operators with RapidMiner Beans}

% author(s)
\author{Christian Bockermann and Hendrik Blom\\ Technical University
  of Dortmund \\ Artificial Intelligence Group \\
  \ttfamily{\{christian.bockermann,hendrik.blom\}@udo.edu}}


\newcommand{\streams}{\textsf{streams}\ }
\newcommand{\plugin}{\textsf{DataStream Plugin}}
\newcommand{\beans}{\textsf{RapidMiner Beans}}

\begin{document}
\maketitle\thispagestyle{empty}

\begin{abstract}
  RapidMiner has become a valuable tool for business intelligence as
  well as in highschool education. Its plugin mechanism allows for
  creating powerful extensions and various plugins exist for
  different applications, data formats and learning schemes.

  In this paper we present the \textsf{RapidMiner Beans} library, a
  simple library and plugin that aims at simplifying the development
  and documentation of custom operators using standard Java
  technologies like annotations and JavaBeans conventions.

  One of the main objectives is to allow for the developer to solely
  focus on the operator development without caring about the house
  keeping required.  For demonstration we give an example
  implementation that shows the benefits of the \textsf{RapidMiner
    Beans}.
\end{abstract}

\section{Introduction}
RapidMiner is a powerful tool for various data analysis and business
intelligence tasks. It provides a wide range of data mining and
machine learning algorithms, data pre-processing operators and
evaluation methods. One of its strongest qualities is the plugin
architecture, which allows for extending RapidMiner with additional
functions using RapidMiner {\em Extensions} or {\em Plugins}.

These plugins add new operators and functions to the core RapidMiner
suite and are implemented within the RapidMiner programming API.  Over
the years this API has evolved to a powerful -- yet sometimes
complicated to learn -- instrument. Besides their core implementation,
operators need to be registered within the operator descriptions file
and various additional meta data files need to be defined for creating
a custom plugin.

When developing extensions or plugins for RapidMiner, we often see
students and new developers struggling with the same hurdles, a very
big one of which is related to the plugin definition files. A second
unpleasant burden is the way to document operators. 

Whereas the former issue can be solved by some investment in setup
time of the development environment, the latter causes the more severe
follow-up as it discourages developers/students to properly document
their achievements. This effectively feeds another hurdle -- usage of
the new operators within the student/RapidMiner community.

With the \textsf{RapidMiner Beans} plugin/library we approach these
issues by
\begin{itemize}
  \item[(a)] lowering the effort required to write and deploy a new operator
  \item[(b)] providing a simple tool for documenting that operator.
\end{itemize}
One of our superior goals was to achieve these objectives by working
as close to the existing path of developing RapidMiner operators as
possible -- just with a little simplification.

In addition to this, we seek to feed some coding guidelines by
following well-accepted conventions such as JavaBeans\cite{JavaBeans}
and the use of Java's annotations mechanism. This allows for creating
new operators by creating a single Java class file as well as
additionally creating a single text file for documentation. Everything
else will be taken care of by the \textsf{RapidMiner Beans} framework.

\bigskip

The rest of this paper is organized as follows: Section
\ref{sec:overview} gives a short high-level overview of the
\textsf{RapidMiner Beans} library and the JavaBean conventions used
within. Following that we will give a walk-through example for
implementing and loading a RapidMiner operator simply with annotations
in Section \ref{sec:operator}. Next we will demonstrate how to
document our operator using the {\em Markdown} wiki-like dialect.
Finally we will give some outlook on future work.

\section{\label{sec:overview}RapidMiner Beans -- Overview}
The RapidMiner plugin meachnism requires operators to be defined
within a descriptive XML file. This file is referenced in RapidMiner
extension files and used to register operators of a plugin at the
start of RapidMiner. For each operator, the XML file contains
\begin{itemize}
  \item the operator's name,
  \item a short description of the operator,
  \item the class implemeting the operator, and
  \item the group in which the operator is displayed within
    RapidMiner.
\end{itemize}

The \beans\ library facilitates two aspects: It is a standalone
library that provides a small set of Java annotations. This allows to
define all of the operator properties directly within the class by
using class-level annotations.

As a second aspect the \beans\ library implements a plugin itself. At
RapidMiner start-up time this plugin will check for classes annotated
with the aforementioned annotations and register these in
RapidMiner. This allows for adding new operators by simply creating
annotated Java classes.

\subsection{JavaBeans and Parameter Annotations}
Partially following the JavaBeans conventions, operators can be
equipped with {\ttfamily get}- and {\ttfamily set}-methods. By
annotating these methods with field- or method-level {\ttfamily @ParameterInfo}
annotations, this allows for automatically extracting RapidMiner
parameter type lists. 

With the {\ttfamily @ParameterInfo} annotation all basic properties of
parameters that are required for displaying parameters within the
RapidMiner user interface can be defined. This further decouples the
operator implementation from the GUI layer aiming towards a cleaner
separation of view and logic.

\subsection{Operator Documentation}
Finally, the documentation of operators is another important aspect of
the \beans\ library. For this, we focus on the simple {\em Markdown}
format, a simple wiki-like markup language that has recently become
popular in portals such as {\ttfamily github.com}, and which easily
translates to HTML.

For documentation, the \beans\ library requires a simple text file to
be created for each operator, with the same name as the operators Java
file but with the {\ttfamily .md}-extension instead.

This simple convention requires two files to be touched for creating a
complete and documented operator: An annotated Java file and a single
Markdown text file. 

\subsection{Support for User-Libraries}
When being added to the RapidMiner plugins directory, the \beans\
library acts as a plugin that will search for external JAR files found
in the {\ttfamily .RapidMiner5/beans} directory of the current user's
home directory. Any JAR file within that directory will be checked for
classes annotated with the {\ttfamily @OperatorInfo} annotation.

This alternate plugin mechanism has proven to be useful especially in
environments where users are not allowed write-access to a
central/shared installation of the RapidMiner suite.


%\newpage
\section{\label{sec:operator}Example -- A simple Operator}
In this section we will outline the implementation of a simple
operator using the \beans\ library. As mentioned above, this requires
the class to be annotated using the \beans\ annotations. Everything
else follows the regular RapidMiner operator implementation.

We will outline the annotations in Section \ref{sec:annotatedOperator}
and will show the use of parameter annotations in Section
\ref{sec:parameters}.

\subsection{\label{sec:annotatedOperator}Operator Annotations}
The sample code in Figure \ref{fig:operator} implements a very basic
RapidMiner operator. The class inherits the {\ttfamily Operator} super
class and implements an almost empty {\ttfamily doWork()} method,
which effectively passes through any input received at its input port.

Apart from the {\ttfamily @OperatorInfo} annotation, present in this
class, the code follows the usual implementation of a RapidMiner
operator. The annotation, however, defines all information that is
required for registering an operator with RapidMiner. With the \beans\
library present in the RapidMiner plugins directory, all classes that
are annotated with {\ttfamily @OperatorInfo} are added to the list of
RapidMiner operators.

\begin{figure}[h!]
  \begin{lstlisting}[numbers=left,stepnumber=1]
    import com.rapidminer.beans.annotations.OperatorInfo;
    import com.rapidminer.beans.annotations.ParameterInfo;

    @OperatorInfo(name="Sample Operator", group="RapidMinerBeans.Example",
                  description="A simple pass-through Operator")
    public class SampleOperator extends Operator {

        Inputport input = getInputPorts().createPort("input");
        OutputPort output = getOutputPorts().createPort( "output" );

        /** Simply pass through the input object to the output port */
        public void doWork(){
           IOObject in = input.getDataOrNull();
           if( in != null ){
              output.deliver(in);
           }
        }      
    }
  \end{lstlisting}
  \caption{\label{fig:operator}A simple RapidMiner operator. Lines 4-5
    do provide the {\ttfamily @OperatorInfo} annotation, defining the
    name, group and tooltip-text of the operator within RapidMiner.}
\end{figure}


\subsection{\label{sec:parameters}Adding Parameters with OperatorBeans}
In this section we will be extending the operator implementation of
Figure \ref{fig:operator} by adding parameters following the JavaBeans
convention. The parameters are annotated using the {\ttfamily
  @ParameterInfo} annotation of the \beans\ library. Figure
\ref{fig:operatorParameter} shows the enhanced example operator with
additional parameters {\ttfamily lambda} and {\ttfamily name}. In the
case of the {\ttfamily lambda} parameter in the example, the
{\ttfamily @ParameterInfo} has been added to the class field of the
operator. Alternatively, it is also possible to add this annotation to
the {\ttfamily set}- or {\ttfamily get}-method as for the {\ttfamily name}
parameter in Figure \ref{fig:operatorParameter}.

\begin{figure}[h!]
  \begin{lstlisting}
    @OperatorInfo(name="Sample Operator", group="RapidMinerBeans.Example",
                  description="A simple pass-through Operator")
    public class SampleOperator extends OperatorBean {
        // port definitions left out for brevity...

        @ParameterInfo( required = true, min = 0.0, max = 1.0, 
                        description = "A threshold value" )
        Double lambda = 0.0; // A parameter of type double

        String name = "";    // A string type parameter

        /** A set-method for the 'lambda' parameter */
        public void setLambda(Double d){
            this.lambda = d;
        }

        /** A set-method for the 'name' parameter */
        @ParameterInfo( required = false,
                        description = "An optional name parameter" )
        public void setName( String name ){
           this.name = name;
        }

        /** The doWork method */
        public void doWork(){
           if( lambda > 0.5 ){ ... }
        }      
    }    
  \end{lstlisting}
  \caption{\label{fig:operatorParameter}A {\ttfamily OperatorBean}
    example, which is a simple operator that provides JavaBean-style
    parameters. The OperatorBean class ensures that all parameters are
    set before the {\ttfamily doWork()} method is executed.}
\end{figure}

The annotations ensure, that the parameter lists of the operator can
be detected. The \beans\ library also provides the necessary methods
to inject the user-entered parameter values into the operator. For
this, the class extends the super class {\ttfamily OperatorBean},
which ensures that all parameters have been set when the {\ttfamily
  doWork()} method is called during operator execution.
The {\ttfamily OperatorBean} class also automatically provides the
parameter type lists, required by RapidMiner to display the required
parameters in the graphical user-interface. An analogue bean also
exists for {\ttfamily OperatorChain}s.  Alternatively, the beans
functionality is provided by the {\ttfamily RapidMinerBeans} class.


\section{\label{sec:documentation}Writing Documentation}
As mentioned before, the \beans\ library uses {\em Markdown}
\cite{markdown} as format for documentation. The documentation is
provided by following a very simple convention:

\medskip

\begin{minipage}{0.95\textwidth}
\noindent
{\em If the operator class is {\ttfamily my.package.Operator}, the
  documentation is expected to be found in a file {\ttfamily
    /my/package/Operator.md} within the classpath}.
\end{minipage}

\medskip

The objective of this simple rule is to encourage developers to
document their operators by simply creating a single additional text
file that provides a textual description.

\subsection{The Markdown Format }
The {\em Markdown} format used by the \beans\ library has been proposed
by John Gruber and is a simple wiki-like dialect that may additionally
integrate HTML tags for any elements not supported by Markdown. Markdown
provides generic support for
\begin{itemize}
  \item lists (numbered and bullet lists)
  \item emphasizing text (italic, bold)
  \item hyperlinks and images
  \item code blocks, block-quotes
\end{itemize}
Markdown has gained a lot of attention as the primary plain-text
format for documentation at {\ttfamily github.com} and became widely
accepted. Multiple tools like {\em pandoc} provide conversion support
from markdown in various other formats.

The \beans\ library uses MarkdownJ \cite{markdownj} to convert
operator documentation to HTML which can then be displayed within
RapidMiner. The following Figure \ref{fig:markdownExample} shows a
sample of Markdown text describing the example operator introduced in
Section \ref{sec:operator}.

\begin{figure}[h!]
\begin{center}
  \begin{minipage}{0.95\textwidth}{\footnotesize
\begin{verbatim}
The Sample Operator
===================

This very simple example operator is used for demonstration purposes
for the RapidMiner Beans library. It simply passes through any input
delivered to its *input*-port.

The extended version provides additional parameters

   - `lambda`
   - `name`

which also do simply serve for demonstration purposes.
\end{verbatim}}
\end{minipage}
\caption{\label{fig:markdownExample}Example markdown documentation for
  the example operator. The major headline is provided by underlining
  a line with equal characters. The {\ttfamily *} character switches to {\em
    italic} style, the back-ticks form {\ttfamily typewrite} code.}
\end{center}
\end{figure}

\subsection{Documentation within RapidMiner}
If operators are documented following the convention mentioned above,
the \beans\ library automatically generates the required operator
documentation at registration time of the corresponding annotated
operator. This documentation is additionally enriched with the list of
parameters created by RapidMiner.  The parameter list documentation in
turn is extracted from the parameter annotations described in Section
\ref{sec:parameters}.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[scale=0.5]{doc-new.png}
    \caption{\label{fig:operatorDoc}Documentation of the {\em Sample
        Operator} operator, which has been written in the {\em
        Markdown} format.}
  \end{center}
\end{figure}


\section{\label{sec:outlook}Summary and Future Work}
We presented the \beans\ library, which provides a simplified way to
create and document new RapidMiner operators by using modern
technologies such as Java annotations and JavaBean conventions. The
plugin mechanism provided by the \beans\ library allows for
automatically loading custom operators from plain JAR files available
in a user's home directory. This is especially useful for developing
and testing custom operators in environments where the central plugin
directory is not writable by users.

For future work we are looking into automatically registering
operators at runtime to support quick code-replacement of classes as
is for example provided in hot-deployment settings such as Tomcat or
JBoss. This will further shorten the development-cycle for new
operators.

Another future direction is additional tool support to convert JAR
files with annotated operator classes into regular RapidMiner plugins
by generating the required XML description files from the annotated
classes.


\paragraph{Acknowledgements} This work was supported by the DFG within
the Collaborative Research Center on {\em Providing Information by
  Resource-Constrained Data Analysis} (SFB-876).

\bibliographystyle{ieeetr}
\bibliography{local-refs,literatur}

\end{document}